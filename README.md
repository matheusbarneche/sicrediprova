# Sicredi Prova Tecnica

## Introduction
the project aims to validate back ends tests using groovy and java.

## Index

<!--ts-->
   * [Introduction](#introduction)
   * [Index](#index)
   * [Technologies](#technologies)
   * [Prerequisites](#prerequisites)
   * [Project run](#project-run)
<!--te-->

## Technologies

* [Groovy (language)](https://groovy-lang.org/)
* [Java (language)](https://www.java.com/pt-BR/)
* [Rest Assured (test framework)](https://rest-assured.io/)
* [Junit (test framework)](https://junit.org/junit5/)
* [Intellij (IDE)](https://www.jetbrains.com/pt-br/idea/)
* [javaFaker (java lib)](https://github.com/DiUS/java-faker)
* [Maven (dependency manager)](https://maven.apache.org/)
* [Docker (contanier)](https://www.docker.com/)

## Prerequisites
1. Download and install jdk(https://www.oracle.com/br/java/technologies/javase-downloads.html)
2. Download and install a IDE(https://www.jetbrains.com/pt-br/idea/download/#section=mac)

## Project run
To build the project insert in the command line : -mvn compile
To run tests insert in the coomand line: -mvn clean test

