package br.com.sicredi.simulacao.testServices

import io.restassured.builder.RequestSpecBuilder
import io.restassured.http.ContentType

class BaseRequest {

    static def getRequestSpec() {
        return new RequestSpecBuilder()
                .setBaseUri("http://localhost:8888")
                .setContentType(ContentType.JSON)
                .build()
    }

}