package br.com.sicredi.simulacao.dataProvider;

import java.util.Locale;

public class FakerTestDataGenerator<firstName> {
    public static <Faker> void main(String[] args) {

        Locale locale = new Locale("pt-BR");
        com.github.javafaker.Faker faker = new com.github.javafaker.Faker(locale);

        String firstName = faker.name().firstName();
        String cpf = CpfGenerator.generate();
        String email = faker.internet().emailAddress();
        Integer payValue = faker.random().nextInt(10, 700);
        Integer installments = faker.random().nextInt(2, 150);
        Boolean insurance = faker.random().nextBoolean();


        System.out.println(firstName);
        System.out.println(cpf);
        System.out.println(email);
        System.out.println(payValue);
        System.out.println(installments);
        System.out.println(insurance);

    }
}
