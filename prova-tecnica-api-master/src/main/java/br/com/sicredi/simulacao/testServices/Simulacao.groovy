package br.com.sicredi.simulacao.testServices

import br.com.sicredi.simulacao.models.SimulacaoCreateRequest
import br.com.sicredi.simulacao.models.SimulacaoEditRequest
import io.restassured.response.ValidatableResponse

import static io.restassured.RestAssured.given

class Simulacao {
    Integer expectedStatusCode
    SimulacaoCreateRequest bodyCreate
    SimulacaoEditRequest bodyEdit
    String cpf = 66414919004
    Integer id = (Integer) Math.random()

    Simulacao(SimulacaoCreateRequest bodyCreate, Integer expectedStatusCode) {
        this.bodyCreate = bodyCreate
        this.expectedStatusCode = expectedStatusCode
    }

    Simulacao(Integer expectedStatusCode) {
        this.expectedStatusCode = expectedStatusCode
    }

    Simulacao(SimulacaoEditRequest bodyEdit, Integer expectedStatusCode) {
        this.bodyEdit = bodyEdit
        this.expectedStatusCode = expectedStatusCode
    }

    ValidatableResponse searchSimulations() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .when()
                .get("/api/v1/simulacoes")
                .then()
                .statusCode(this.expectedStatusCode)
    }

    ValidatableResponse createSimulations() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .body(this.bodyCreate)
                .when()
                .post("/api/v1/simulacoes")
                .then()
                .statusCode(this.expectedStatusCode)
    }

    ValidatableResponse searchSimulationsByCpf() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .when()
                .get("/api/v1/simulacoes/" + cpf + "")
                .then()
                .statusCode(this.expectedStatusCode)
    }

    ValidatableResponse searchInvalidSimulationsByCpf() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .when()
                .get("/api/v1/simulacoes/3u3idjdiwjid")
                .then()
                .statusCode(this.expectedStatusCode)
    }

    ValidatableResponse editSimulations() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .body(this.bodyEdit)
                .when()
                .put("/api/v1/simulacoes/" + cpf + "")
                .then()
                .statusCode(this.expectedStatusCode)
    }

    ValidatableResponse deleteSimulations() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .when()
                .delete("/api/v1/simulacoes/" + id + "")
                .then()
                .statusCode(this.expectedStatusCode)
    }

    ValidatableResponse deleteInvalidSimulations() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .when()
                .delete("/api/v1/simulacoes/x")
                .then()
                .statusCode(this.expectedStatusCode)
    }
}
