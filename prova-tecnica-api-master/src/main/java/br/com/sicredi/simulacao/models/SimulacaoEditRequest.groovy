package br.com.sicredi.simulacao.models

import br.com.sicredi.simulacao.testServices.Simulacao
import com.github.javafaker.Faker

import static org.hamcrest.CoreMatchers.is

class SimulacaoEditRequest {
    String nome
    String cpf
    String email
    Double valor
    Double parcelas
    Boolean seguro

    SimulacaoEditRequest() {
        Locale locale = new Locale("pt-BR")
        Faker faker = new Faker(locale)

        String firstName = faker.name().firstName()
        String cpfFaker = new Simulacao(200).searchSimulations()
                .body("id[0]", is(11)).extract().path("cpf[0]")
        String emailFaker = faker.internet().emailAddress()
        Integer payValue = faker.random().nextInt(10, 700)
        Integer installments = faker.random().nextInt(2, 150)
        Boolean insurance = faker.random().nextBoolean()

        this.nome = firstName
        this.cpf = cpfFaker
        this.email = emailFaker
        this.valor = payValue
        this.parcelas = installments
        this.seguro = insurance
    }

    SimulacaoEditRequest(String nome, String cpf, String email, Double valor, Double parcelas, Boolean seguro) {
        this.nome = nome
        this.cpf = cpf
        this.email = email
        this.valor = valor
        this.parcelas = parcelas
        this.seguro = seguro
    }

    Boolean getSeguro() {
        return seguro
    }

    void setSeguro(Boolean seguro) {
        this.seguro = seguro
    }
}
