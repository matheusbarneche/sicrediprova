package br.com.sicredi.simulacao.models

import br.com.sicredi.simulacao.dataProvider.CpfGenerator

class RestricaoSearchRequest {
    private String cpf

    RestricaoSearchRequest() {
        this.cpf = CpfGenerator.generate()
    }
}
