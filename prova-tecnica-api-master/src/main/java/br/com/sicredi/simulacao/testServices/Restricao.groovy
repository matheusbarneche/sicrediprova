package br.com.sicredi.simulacao.testServices


import io.restassured.response.ValidatableResponse

import static io.restassured.RestAssured.given

class Restricao {
    Integer expectedStatusCode
    String cpf = "84809766080"

    Restricao(Integer expectedStatusCode) {
        this.expectedStatusCode = expectedStatusCode
    }

    ValidatableResponse search() {
        return given()
                .spec(BaseRequest.getRequestSpec())
                .when()
                .get("/api/v1/restricoes/" + cpf + "")
                .then()
                .statusCode(this.expectedStatusCode)
    }
}
