package br.com.sicredi.digital.contractTest

import br.com.sicredi.simulacao.models.SimulacaoCreateRequest
import br.com.sicredi.simulacao.models.SimulacaoEditRequest
import br.com.sicredi.simulacao.testServices.Restricao
import br.com.sicredi.simulacao.testServices.Simulacao
import org.junit.Test

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema

class ContractTest {

    String schemaPath = "src/test/resources/schemas"

    @Test
    void searchCpfWithNoRestriction() {
        new Restricao(200).search()
                .body(matchesJsonSchema(new File(schemaPath + "/restricao_search_schema.json")))
    }

    @Test
    void searchValidSimulations() {
        new Simulacao(200).searchSimulations()
                .body(matchesJsonSchema(new File(schemaPath + "/simulacao_search_schema.json")))
    }

    @Test
    void createValidSimulations() {
        new Simulacao(new SimulacaoCreateRequest(), 201).createSimulations()
                .body(matchesJsonSchema(new File(schemaPath + "/simulacao_create_schema.json")))
    }

    @Test
    void seachValidSimulatiosByCpf() {
        new Simulacao(200).searchSimulationsByCpf()
                .body(matchesJsonSchema(new File(schemaPath + "/simulacao_search_by_cpf_schema.json")))
    }

    @Test
    void editSimulations() {
        new Simulacao(new SimulacaoEditRequest(), 200).editSimulations()
                .body(matchesJsonSchema(new File(schemaPath + "/simulacao_edit_schema.json")))
    }
}
