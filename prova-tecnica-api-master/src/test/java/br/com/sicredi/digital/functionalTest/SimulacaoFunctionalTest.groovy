package br.com.sicredi.digital.functionalTest

import br.com.sicredi.simulacao.models.SimulacaoCreateRequest
import br.com.sicredi.simulacao.models.SimulacaoEditRequest
import br.com.sicredi.simulacao.testServices.Simulacao
import org.junit.Test

import static org.hamcrest.CoreMatchers.is

class SimulacaoFunctionalTest {

    @Test
    void createSimulations() {
        new Simulacao(new SimulacaoCreateRequest("teste", "teste", "teste@mail.com", 90000, 12, true), 400).createSimulations()
                .body("erros.valor", is("Valor deve ser menor ou igual a R\$ 40.000"))
    }

    @Test
    void getSimulatiosByCpf() {
        new Simulacao(404).searchInvalidSimulationsByCpf()
    }

    @Test
    void editSimulations() {
        new Simulacao(new SimulacaoEditRequest("teste", "66414919004", "teste@mail.com", 9000000, 12, true), 404).editSimulations()
    }

    @Test
    void deleteSimulations() {
        new Simulacao(400).deleteInvalidSimulations()
    }
}
