package br.com.sicredi.digital.acceptanceTest

import br.com.sicredi.simulacao.models.SimulacaoCreateRequest
import br.com.sicredi.simulacao.models.SimulacaoEditRequest
import br.com.sicredi.simulacao.testServices.Simulacao
import org.junit.Test

import static org.hamcrest.CoreMatchers.is

class SimulacaoTest {

    @Test
    void getSimulations() {
        new Simulacao(200).searchSimulations()
                .body("id[0]", is(11))
    }

    @Test
    void createSimulations() {
        new Simulacao(new SimulacaoCreateRequest(), 201).createSimulations()
    }

    @Test
    void getSimulatiosByCpf() {
        new Simulacao(200).searchSimulationsByCpf()
    }

    @Test
    void editSimulations() {
        new Simulacao(new SimulacaoEditRequest(), 200).editSimulations()
    }

    @Test
    void deleteSimulations() {
        new Simulacao(200).deleteSimulations()
    }
}
