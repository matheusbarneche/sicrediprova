package br.com.sicredi.digital.healthCheck

import br.com.sicredi.simulacao.testServices.BaseRequest
import org.junit.Test

import static io.restassured.RestAssured.given

class HealthCheckTest {
    @Test
    void healthCheckActuator() {
        given().
                spec(BaseRequest.getRequestSpec()).
                get("/v2/api-docs").
                then().
                statusCode(200)
    }

}